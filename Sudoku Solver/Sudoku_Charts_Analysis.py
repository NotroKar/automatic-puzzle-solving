from Crooks_Algorithm_Sudoku_Solver import *
from Backtracking_Sudoku_Solver import *
from math import *
import numpy as np
import random as random
import matplotlib.pyplot as plt
import copy
import time
import matplotlib

matplotlib.rcParams.update({'font.size': 20}) #Updates the font size for all text

def round_sig(x, sig=4): #Function to round to 4 significant figures
    """Rounds a number to 4 significant figures"""
    return round(x, sig-int(floor(log10(abs(x))))-1)

def analysis(difficulty):
    """Creates a comparison of solving times based on difficulty """ 
    iterations = [] # Counting the amount of guess that are made 
    iterations2 = [] # Counting the amount of guess that are made 
    
    grids = [] # Initialising the list of grids to be solved for analysis
    times = [] # Initialising the list of times for crooks

    grids = generate_puzzles(difficulty,10) # generate puzzles
    nextgrid = copy.deepcopy(grids) #copy puzzles for the second round of solving as setup()
                                    #permanently mutates grids 
    
    for i in grids : # Begins recording the times of the puzzles in "grids" to be solved
        start_time = time.time()
        x = setup(i) #Calls crooks algorithm
        end_time = time.time()
        duration = (end_time - start_time)
        times.append(duration)
        iterations.append(x) 
    
    times2 = []#intialsing the list of times for backtracker
    
    for i in nextgrid :
        start_time = time.time()
        y = solve(i)#Calls the backtracking algorithm
        end_time = time.time()
        duration = (end_time - start_time)
        times2.append(duration)
        iterations2.append(y)

    labels = ["1","2","3","4","5","6","7","8","9","10"] # Test case labels
    x = np.arange(len(labels))  #location of the labels 
    width = 0.35  # the width of the bars in the bar graphs

    fig, ax = plt.subplots()
    
    rectangles1 = ax.bar(x - width/2, times, width, label="Crook's",color = "C0")
    rectangles2 = ax.bar(x + width/2, times2, width, label="Backtracking",color = "C1")

    ax.set_ylabel('Time Taken in Seconds')
    ax.set_title("Crook's Algorithm and Backtracking for Hard puzzles",fontsize = 20) #enlargen the fontsize here
    ax.set_xlabel("Test Case")
    ax.set_xticks(x)
    ax.set_xticklabels(labels) #Makes sure that the test case labels are in the correct position
    plt.yscale("log") #Plotting on a logarithmic timescale
    ax.legend(loc = "center right")

    label_each_bar(rectangles1,ax)
    label_each_bar(rectangles2,ax)

    plt.show() # Display the plot

def label_each_bar(rectangles,ax):
    """Produces a label above each bar/rectangle displaying its value."""
    for rectangle in rectangles:
        height = rectangle.get_height()
        if height != 0 :
            height = round_sig(height)

        ax.annotate('{}'.format(height),size = 14,
                    xy=(rectangle.get_x() + rectangle.get_width() / 2,height),
                    xytext=(0, 3),  # 3 increments above each bar
                    textcoords="offset points",
                    ha='center', va='bottom')
            





