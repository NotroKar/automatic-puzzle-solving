import random
total_it = 0 #Counts the amount of guesses made
start_list = [[0,0],[0,3],[0,6],[3,0],[3,3],[3,6],[6,0],[6,3],[6,6]]
  #starting index for each of the boxes

print("Backtracking Sudoku Solver")
      
def solve_Rec(grid):
    """The recursive function used to make guesses and solve the grid"""
    
    global total_it
    square_index = find_empty(grid)
    if square_index == 81: #base case, If this is satisfied then we have found our solution
        return grid
    x = square_index % 9  # finds the row index 
    y = square_index // 9 # rounds the result down to the nearest whole number and finds the column index

    for i in range(1,10): #Begins guessing the values 1-9 for each empty cell it identifies
        
        test_num = check_num(x,y,i,grid)
        
        if test_num == True :
            
            total_it = total_it + 1
            grid[y][x] = i # Sets the empty cell to the valid values
            
            if solve_Rec(grid): # Calls the function recursively
                return grid  
            grid[y][x] = 0 # If the guess is wrong then set the guess back to 0 indicating empty
            
    return False            

def print_sudoku(sudoku):  # Function to print sudoku in a legible format.
    """Function to print the sudoku grid in a legible format, mainly used for debugging"""
    for i in range(len(sudoku[0])):
        print(i, sudoku[i])
        

def display(x): #display the sudoku within a grid
    """Displays the grid in an aesthetically pleasing grid"""
    
    print("╔═══╤═══╤═" + "══╦══" + "═╤══" * 2 + "═╦══" +  "═╤═══╤═══╗")
    for row in range(9):
        disp = "║ "
        for col in range(9):
            if (col + 1) % 3 == 0:
                disp += str(x[row][col]) + " ║ " #for the 3x3 box borders
            else: 
                disp += str(x[row][col]) + " │ "
        print(disp)
        if row < 8:
            if (row + 1) % 3 == 0: #for the 3x3 box corners
                print("╠═══╪═══╪═" + "══╬══" + "═╪══" * 2 + "═╬══" +  "═╪═══╪═══╣")
            else:
                print("╟───┼───┼───╫─" + "──┼─" * 2 + "──╫───┼───┼───╢") #for the 3x3 box borders
        else:
            print("╚═══╧═══╧═" + "══╩══" + "═╧══" * 2 + "═╩══" +  "═╧═══╧═══╝")
            

    
            
def check_num(x,y,test_num,grid):
    """ Return True if the test_num at the index represented by x and y is valid""" 
    
    for j in range(9): #Cycles through the rows to check for any occurance of the test_num
        if grid[j][x] == test_num :
            return False
            
    for k in range(9):#Cycles through the column to check for any occurance of the test_num
        if grid[y][k] == test_num :
            return False
            
    xpos = (x//3)*3
    ypos = (y//3)*3
    
    for l in range(3): #Cycles through the box to check for any occurance of the test_num
        for r in range(3):
            if grid[ypos+l][xpos+r] == test_num :
                return False
    return True
            
      

def find_empty(grid):
    """Returns the square_index of an empty cell, if there are none then return 81 in the grid"""
    
    for i in range(9):
        for j in range(9):
            if grid[i][j] == 0 :
                square_index = (i*9 + (j)) # formula to determine the square_index from a 2D coordinate system
                
                return square_index
    return 81 

def check_solution(grid):
    """Determines if the grid is a valid solution to a sudoku puzzle"""
    
    complete = True #Assume grid is valid unless proven otherwise 
    
    for i in range(9): #Checks if the numbers 1-9 are in each row
        check_list = [1,2,3,4,5,6,7,8,9]
        for j in range(9):
            if grid[i][j] in check_list :
                check_list.remove(grid[i][j])
            
        if len(check_list) != 0 :
                complete = False


    for i in range(9):#Checks if the numbers 1-9 are in each column
        check_list = [1,2,3,4,5,6,7,8,9]
        for j in range(9):
            if grid[j][i] in check_list :
                check_list.remove(grid[j][i])
            
        if len(check_list) != 0 :
            complete = False

    start_list = [[0,0],[0,3],[0,6],[3,0],[3,3],[3,6],[6,0],[6,3],[6,6]]
    for k in range(9): #Checks if the numbers 1-9 are in each box
        check_list = [1,2,3,4,5,6,7,8,9]
        i = start_list[k][0]
        while i < (start_list[k][0]+3):
            j = start_list[k][1]
            while j < (start_list[k][1]+3):
                
                if grid[i][j] in check_list :
                    check_list.remove(grid[i][j])
                j = j+1
            i=i+1
        

        if len(check_list) != 0 :
            complete = False
                    
    return(complete)

def solve(grid):
    """Responsible for setting intial variables and outputting the finished results"""
    global total_it
    
    total_it = 0
    solved = solve_Rec(grid) #Begins the recursive solving
    
    if solved != False : #As our solve_Rec function returns a Boolean or a list this is necessary
        #display(solved) #Displays the solution in an aesthetically pleasing way 
        return grid
    else:
        return False

    
def generate():
    """Generates random solved grids via bruteforce"""
    grid = [[0 for i in range(1,10)] for i in range(1,10)] 
    grid[0] = [i for i in range(1,10)] 
    random.shuffle(grid[0]) #randomises the first row
    
    for row in range(1,9): #removes all invalid choices for each cell and randomly picks from remaining numbers
        for col in range(9):
            options = [i for i in range(1,10)] #resets options for current cell too all values 
            
            for i in grid[row]:
                if i in options:
                    options.remove(grid[row][grid[row].index(i)]) #remove all numbers already within the row
                    
            for i in grid:
                if i[col] in options:
                    options.remove(grid[grid.index(i)][col]) #remove all numbers already within the column
                    
            xbox = col // 3 #locate which 3x3 box we are in
            ybox = row // 3
                    
            for i in range(3):
                for j in range(3):
                    if grid[ybox * 3 + i][xbox * 3 + j] in options:
                        options.remove(grid[ybox * 3 + i][xbox * 3 + j]) #remove all numbers already within the 3x3
                
            try: #tries to pick a random numbers from remaining choices
                grid[row][col] = random.choice(options)
            except: #if no possible choices exist restart the process
                return False
        
    return grid

def generate_puzzles(difficulty,n):
    """Generate n amount of puzzles with difficulty "very easy", "easy", "medium", "hard" , "very hard".
"""
    generatedpuzzles = []
    grids = []
    while len(generatedpuzzles) < n : 
        x = generate()
        if x != False : # Determines if the grid creating is a valid sudoku
            if check_solution(x) == True :
                generatedpuzzles.append(x) #Our list of generated solved puzzles

    for i in range(len(generatedpuzzles)):
        grids.append(puzzle_stripper(generatedpuzzles[i],difficulty))
        #Turns the solved grids into sudoku puzzles for the user

    return grids
    

def puzzle_stripper(grid,difficulty):
    """Removes values from cells and leaves them blank depending on the difficulty chosen"""
    start_list = [[0,0],[0,3],[0,6],[3,0],[3,3],[3,6],[6,0],[6,3],[6,6]]
    
    if difficulty == "very easy" : #Difficulty setting 
        occurance_of_each_num = 7 # Criteria that fits a "very easy" puzzle
        remove_list = []
        for i in range(9):
            remove_list. append([i+1]*(9-occurance_of_each_num))# lists of length 7 for each occurance of number
        
        for i in range(len(remove_list)):
            
            for j in range(len(remove_list[i])):
                flag = False #flag is used here to make sure we always remove a number from somwehere in the grid
        
                while flag == False :
                    boxremove = random.randint(0,8)#Index of random integer to be removed
                    k = start_list[boxremove][0]
                    while k < (start_list[boxremove][0]+3):#Cycles through each box removing the integer
                        l = start_list[boxremove][1]
                        while l < (start_list[boxremove][1]+3):
                            if grid[k][l] in remove_list[i] :
                                remove_list[i].remove(grid[k][l])
                                grid[k][l] = 0
                                flag = True #An integer has been removed
                            l = l + 1
                        k = k + 1

    if difficulty == "easy" : #Difficulty setting 
        occurance_of_each_num = 5 #criteria that fits an "easy" puzzle
        remove_list = []
        for i in range(9):
            remove_list. append([i+1]*(9-occurance_of_each_num))# lists of length 7 for each occurance of number
        
        for i in range(len(remove_list)):
            
            for j in range(len(remove_list[i])):
                flag  = False #flag is used here to make sure we always remove a number from somwehere
                while flag == False :
                    boxremove = random.randint(0,8)#Index of random integer to be removed
                    #print(boxremove)
                    k = start_list[boxremove][0]
                    while k < (start_list[boxremove][0]+3): #Cycles through each box removing the integer
                        l = start_list[boxremove][1]
                        while l < (start_list[boxremove][1]+3):
                            if grid[k][l] in remove_list[i] :
                                remove_list[i].remove(grid[k][l])
                                grid[k][l] = 0
                                flag = True #An integer has been removed
                            l = l + 1
                        k = k + 1
        

    if difficulty == "medium" :
        occurance_of_first5_num = 4 #Criteria that fits a "medium" puzzle
        occurance_of_last4_num = 3
        remove_list = []
        for i in range(5):
            remove_list.append([i+1]*(9-occurance_of_first5_num))
        for i in range(5,9):
            remove_list.append([i+1]*(9-occurance_of_last4_num))# lists of length 7 for each occurance of number
        print(remove_list)
        for i in range(len(remove_list)):
            
            for j in range(len(remove_list[i])):
                flag = False
                while flag == False:
                    boxremove = random.randint(0,8) #Index of random integer to be removed
                    
                    k = start_list[boxremove][0]
                    while k < (start_list[boxremove][0]+3): #Cycles through each box removing the integer
                        l = start_list[boxremove][1]
                        while l < (start_list[boxremove][1]+3):
                            if grid[k][l] in remove_list[i] :
                                remove_list[i].remove(grid[k][l])
                                grid[k][l] = 0
                                flag = True #An integer has been removed
                            l = l + 1
                        k = k + 1
                        
        
        
    if difficulty == "hard" :

        remove_list = [] 
        remove_list.append((9-1)*[1]) #Criteria that fits a "hard" puzzle
        remove_list.append((9-2)*[2])
        remove_list.append((9-3)*[3])
        remove_list.append((9-3)*[4])
        remove_list.append((9-3)*[5])
        remove_list.append((9-4)*[6])
        remove_list.append((9-4)*[7])
        remove_list.append((9-4)*[8])
        remove_list.append((9-4)*[9])
        
        for i in range(len(remove_list)):
            
            for j in range(len(remove_list[i])):
                flag = False
                while flag == False : 
                    boxremove = random.randint(0,8) #Index of random integer to be removed
        
                    k = start_list[boxremove][0]
                    while k < (start_list[boxremove][0]+3): #Cycles through each box removing the integer
                        l = start_list[boxremove][1]
                        while l < (start_list[boxremove][1]+3):
                            if grid[k][l] in remove_list[i] :
                                remove_list[i].remove(grid[k][l])
                                grid[k][l] = 0
                                flag = True #An integer has been removed
                            l = l + 1
                        k = k + 1        
    

    if difficulty == "very hard" :

        remove_list = []
        remove_list.append((9-1)*[1]) #Criteria that fits a "very hard" puzzle
        remove_list.append((9-1)*[2])
        remove_list.append((9-2)*[3])
        remove_list.append((9-2)*[4])
        remove_list.append((9-3)*[5])
        remove_list.append((9-3)*[6])
        remove_list.append((9-3)*[7])
        remove_list.append((9-3)*[8])
        remove_list.append((9-4)*[9])
        
        for i in range(len(remove_list)):
            for j in range(len(remove_list[i])):
                flag = False
                while flag == False : 
                    boxremove = random.randint(0,8) #Index of random integer to be removed
                    k = start_list[boxremove][0]
                    while k < (start_list[boxremove][0]+3): #Cycles through each box removing the integer
                        l = start_list[boxremove][1]
                        while l < (start_list[boxremove][1]+3):
                            if grid[k][l] in remove_list[i] :
                                remove_list[i].remove(grid[k][l])
                                grid[k][l] = 0
                                flag = True #An integer has been removed
                            l = l + 1
                        k = k + 1
        
    return grid 

            


    
    
        
        

