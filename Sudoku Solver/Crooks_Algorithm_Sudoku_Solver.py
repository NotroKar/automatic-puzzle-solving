import copy

finalgridsolved = list()   #Location of our grid if it is to be solved
total_it = 0                #Responsible for counting the amount of guesses made for analysis purposes
box_list = [[0, 1, 2, 9, 10, 11, 18, 19, 20],    [3, 4, 5, 12, 13, 14, 21, 22, 23],   [6, 7, 8, 15, 16, 17, 24, 25, 26],
            [27, 28, 29, 36, 37, 38, 45, 46, 47],[30, 31, 32, 39, 40, 41, 48, 49, 50],[33, 34, 35, 42, 43, 44, 51, 52, 53],
            [54, 55, 56, 63, 64, 65, 72, 73, 74],[57, 58, 59, 66, 67, 68, 75, 76, 77],[60, 61, 62, 69, 70, 71, 78, 79, 80]]
            #The index of each cell within a box, The index of each list corresponds to the box on the sudoku grid

start_list = [[0,0],[0,3],[0,6],[3,0],[3,3],[3,6],[6,0],[6,3],[6,6]] #Represents the index of the starting points when analysing boxes
print("Crook's Algorithm Sudoku Solver")

def solve_Rec_crook(grid):
    """Recursive function respomsible for calling relevant functions and making the guesses if required to solve the sudoku puzzle.
    
"""
    
    global finalgridsolved
    global total_it
    
    x = markup(grid) 
    numberlist = []
    
    for k in range(len(x)):
        numberlist.append(list(x[k]))  #Converts a list of sets into a list of list for processing reasons
        
    applycrooks(grid)
    
    square_index = find_empty(grid)

    if square_index == 81:  #base case, If this is satisfied then we have found our solution
        finalgridsolved = copy.deepcopy(grid)
        return grid
    
    x = square_index % 9  # finds the row index 
    y = square_index // 9 # rounds the result down to the nearest whole number and finds the column index

    savednumberlist = copy.deepcopy(numberlist) #storing our current markup in case of a incorrect guess
    
    for i in range(len(numberlist[square_index])):
        
        test_num = check_num(x,y,numberlist[square_index][i],grid)  
        
        if test_num == True :
            
            total_it = total_it + 1
            savedgrid2 = copy.deepcopy(grid)
            grid[y][x] = numberlist[square_index][i]
            savedgrid = copy.deepcopy(grid)
            numberlist1 = markup(savedgrid)
            #All of the above are in case of an incorrect guess, we will refer back to these stored variables
            
            if set() in numberlist1 :  #Determines if the guess has created an insolvable puzzle
                grid[y][x] = 0         #reset the guess
            
            else :
        
                if solve_Rec_crook(grid):  
                    return grid
            
                grid = copy.deepcopy(savedgrid2)
                numberlist = copy.deepcopy(savednumberlist)
                grid[y][x] = 0
                #This all resets the grid in case of an incorrect guess also
                
    return False


def print_sudoku(sudoku):  # Function to print sudoku in a legible format.
    """Function to print the sudoku grid in a legible format, mainly used for debugging"""
    for i in range(len(sudoku[0])):
        print(i, sudoku[i])

            
def check_num(x,y,test_num,grid):
    """ Return True if the test_num at the index represented by x and y is valid""" 
    
    for j in range(9): #Cycles through the rows to check for any occurance of the test_num
        if grid[j][x] == test_num :
            return False
            
    for k in range(9):#Cycles through the column to check for any occurance of the test_num
        if grid[y][k] == test_num :
            return False
            
    xpos = (x//3)*3
    ypos = (y//3)*3
    
    for l in range(3): #Cycles through the box to check for any occurance of the test_num
        for r in range(3):
            if grid[ypos+l][xpos+r] == test_num :
                return False
    return True
            

def find_empty(grid):
    """Returns the square_index of an empty cell, if there are none then return 81 in the grid"""
    
    for i in range(9):
        for j in range(9):
            if grid[i][j] == 0 :
                square_index = (i*9 + (j)) # formula to determine the square_index from a 2D coordinate system
                
                return square_index
    return 81 

def check_solution(grid):
    """Determines if the grid is a valid solution to a sudoku puzzle"""
    
    complete = True #Assume grid is valid unless proven otherwise 
    
    for i in range(9): #Checks if the numbers 1-9 are in each row
        check_list = [1,2,3,4,5,6,7,8,9]
        for j in range(9):
            if grid[i][j] in check_list :
                check_list.remove(grid[i][j])
            
        if len(check_list) != 0 :
                complete = False


    for i in range(9):#Checks if the numbers 1-9 are in each column
        check_list = [1,2,3,4,5,6,7,8,9]
        for j in range(9):
            if grid[j][i] in check_list :
                check_list.remove(grid[j][i])
            
        if len(check_list) != 0 :
            complete = False

    start_list = [[0,0],[0,3],[0,6],[3,0],[3,3],[3,6],[6,0],[6,3],[6,6]]
    for k in range(9): #Checks if the numbers 1-9 are in each box
        check_list = [1,2,3,4,5,6,7,8,9]
        i = start_list[k][0]
        while i < (start_list[k][0]+3):
            j = start_list[k][1]
            while j < (start_list[k][1]+3):
                
                if grid[i][j] in check_list :
                    check_list.remove(grid[i][j])
                j = j+1
            i=i+1
        

        if len(check_list) != 0 :
            complete = False
                    
    return(complete)


def markup(grid):
    """Determines the values that a cell can take, returns an array of arrays of length 81"""
    start_list = [[0,0],[0,3],[0,6],[3,0],[3,3],[3,6],[6,0],[6,3],[6,6]]
    numberlist = [set([1,2,3,4,5,6,7,8,9]) for i in range(9) for j in range(9)] #intialise each cell can take all values
    
    for i in range(9):
        for j in range(9):
            
            if grid[i][j] != 0 : # for cells that are already taken set their value to 0 as to not process them in the future 
                numberlist[i*9 +j].clear()
                numberlist[i*9 +j].add(0)
               
            
            if i <= 2 and j <= 2 :
                start = start_list[0]
            elif i <= 2 and i >= 0 and j <= 5 and j >= 3:
                start = start_list[1]
            elif i <= 2 and i >= 0 and j <= 8 and j >= 6:
                start = start_list[2]
            elif i <= 5 and i >= 3 and j <= 2 and j >= 0:
                start = start_list[3]
            elif i <= 5 and i >= 3 and j <= 5 and j >= 3:
                start = start_list[4]
            elif i <= 5 and i >= 3 and j <= 8 and j >= 6:
                start = start_list[5]
            elif i <= 8 and i >= 6 and j <= 2 and j >= 0:
                start = start_list[6]
            elif i <= 8 and i >= 6 and j <= 5 and j >= 3:
                start = start_list[7]
            elif i <= 8 and i >= 6 and j <= 8 and j >= 6:
                start = start_list[8]

            #The above list of conditional statements ensures when processing a box the correct cells are processed
            
            for k in range(9):
                
                if grid[i][k] != 0 and grid[i][k] in numberlist[i*9 +j]: #Removes values that already occur within the same row as the chosen cell

                    numberlist[i*9 + j].remove(grid[i][k])
                    
                if grid[k][j] != 0 and grid[k][j] in numberlist[i*9 +j]: #Removes values that already occur within the same column as the chosen cell

                    numberlist[i*9 + j].remove(grid[k][j])
                    
                f = start[0]
                while f < (start[0]+3): #Removes values that already occur within the same row as the chosen box
                    g = start[1]
                    while g < (start[1]+3):

                        if (i*9 + j) not in box_list[start_list.index(start)]:

                            box_list[start_list.index(start)].append(i*9+j)
                            
                        if grid[f][g] != 0 and grid[f][g] in numberlist[i*9+j]:
                            numberlist[i*9 + j].remove(grid[f][g])
                            
                        g = g+1
                        
                    f = f+1
                                                     
    return numberlist
    
                        
def crooksrow(grid,numberlist): # Does crooks algorithm on the rows
    """Determines if a preemptive sets exists in any row"""
    #print("called crooks")
    #Bruteforcer needs to be written for all combinations of these sets. If a set is found then the eliminate_preemptive is updates and we redo the process.
    #Hard Logic here but possible.

    for i in range(9): #Iterating through each row in the grid
        start = copy.deepcopy(i)
        for j in range(9):#Iterating throught the current row
            templist = []
            for k in range(9):
                if 0 not in numberlist[start*9 + k] :
                    templist.append(set(numberlist[start*9 +k])) #creates a templist for the given row
                          
            x = list(find_permutations(templist)) # templist is the list of the markup for each cell
            
            if x != [[]] :
                
                for i in range(len(x)): # runs through each permutation and works out the union of the list of sets within the set
                    temp = x[i][0]

                    for j in range(len(x[i])):
                        temp = temp.union(x[i][j])
                        
                        if j+1 < len(x[i]):
                            if len(temp) == j+1 and len(temp)>1 : #If the union of elements of the set with another set is the same length of the amount of sets then we have a preemptive set
                                
                                eliminate_preemptive(temp,start,numberlist,"row",grid)
                                return grid
                                
                j = 8
                i = 8
                            
    return grid
                    
                
            
                           
def crookscolumn(grid,numberlist): # does crooks algorithm on the columns
    """Determines if a preemptive sets exists in any column"""
    
    for i in range(9):  #Iterating through each column in the grid
        for j in range(9): #Iterating through the current column in the grid
            start = copy.deepcopy(j)
            templist = []
            for k in range(9):
                
                if 0 not in numberlist[start + 9*k] :
                    templist.append(set(numberlist[start + 9*k])) #creates a templist for the given column
            
            
            x = list(find_permutations(templist)) # templist is the list of sets that each cell could utilise   
            
            if x != [[]] :
                
                for i in range(len(x)): # runs through each permutation and works out the union of the list of sets within the set
                    
                    temp = x[i][0]
                    for j in range(len(x[i])):
                        temp = temp.union(x[i][j])
                        
                        if j+1 < len(x[i]):
                            if len(temp) == j+1 and len(temp)>1 : #If the union of elements of the set with another set is the same length of the amount of sets then we have a preemptive set
                                

                                eliminate_preemptive(temp,start,numberlist,"col",grid)
                                return grid
                j = 8
                i = 8
                
    return grid

def crooksbox(grid,numberlist): #Does crooks algorithm on the 3x3 boxes
    """Determines if a preemptive sets exists in any box"""

    for i in range(9): #Iterates through each box in the grid
        
        for j in range(9): #Iterates through the current box in the grid
            templist = []
            

            if i <= 2 and j <= 2 :
                start = start_list[0]
            elif i <= 2 and i >= 0 and j <= 5 and j >= 3:
                start = start_list[1]
            elif i <= 2 and i >= 0 and j <= 8 and j >= 6:
                start = start_list[2]
            elif i <= 5 and i >= 3 and j <= 2 and j >= 0:
                start = start_list[3]
            elif i <= 5 and i >= 3 and j <= 5 and j >= 3:
                start = start_list[4]
            elif i <= 5 and i >= 3 and j <= 8 and j >= 6:
                start = start_list[5]
            elif i <= 8 and i >= 6 and j <= 2 and j >= 0:
                start = start_list[6]
            elif i <= 8 and i >= 6 and j <= 5 and j >= 3:
                start = start_list[7]
            elif i <= 8 and i >= 6 and j <= 8 and j >= 6:
                start = start_list[8]
            
            for k in box_list[start_list.index(start)]:
                
                if 0 not in numberlist[k] :
                    templist.append(set(numberlist[k])) # creates a templist of the markup values for the current box
                    
            x = list(find_permutations(templist)) # templist is the list of sets that each cell could utilise   

            if x != [[]] :
                
                for i in range(len(x)): # runs through each permutation and works out the union of the list of sets within the set
                    temp = x[i][0]
                    for j in range(len(x[i])):
                        temp = temp.union(x[i][j])
                        
                        if j+1 < len(x[i]):
                            if len(temp) == j+1  and len(temp) > 1  : #If the union of elements of the set with another set is the same length of the amount of sets then we have a preemptive set
                                
                            
                                eliminate_preemptive(temp,start,numberlist,"box",grid)
                                return grid
                j = 8
                i = 8

    return grid
                                
                    
                
def find_permutations(elements): # function that makes a 2D array of lists of each permutation of sets.
    """Finds all the permutations of the list elements"""
    
    if len(elements) <=1: #If our list only has length 1 
        yield elements  
    else:
        for perm in find_permutations(elements[1:]): # Recursive function call for each element in elements
            for i in range(len(elements)):
                yield perm[:i] + elements[0:1] + perm[i:]

            
def eliminate_preemptive(superset,start,numberlist,col_box_row,grid): #uses the preemptive sets to reduce the values that the surrounding cells could take
    """Eliminates the values outside of the cells that induced the preemptive set
       Parameters : superset = our preemptive set
                    start = index to start removing values in the preemptive set
                    numberlist = a list of the markup of each cell
                    col_box_row = string represting if we found a preemptive set in a column, box or row
                    grid = current Sudoku puzzle
"""


    if col_box_row == "row" :
        for i in range(9):
            if numberlist[start*9 +i].issubset(superset) == False:
                numberlist[start*9 + i] = numberlist[start*9+i] - (superset) #Removes the values in the preemptive set from the cells not used in the preemptive set
                
    if col_box_row == "col" :
        for j in range(9):
            if numberlist[start + 9*j].issubset(superset) == False:
                numberlist[start + 9*j] = numberlist[start+9*j] - (superset) #Removes the values in the preemptive set from the cells not used in the preemptive set

    if col_box_row == "box" :
        for k in box_list[start_list.index(start)] :
            if numberlist[k].issubset(superset) == False:
                numberlist[k] = numberlist[k] - (superset) #Removes the values in the preemptive set from the cells not used in the preemptive set
      
    fillsingles(grid,numberlist)
    
            
def fillsingles(grid,numberlist): #function to fill in the singularities in the sudoku puzzle
    """Determines if any of the cells can only have a single value, if so then place this value in this cell"""
    
    #singlecomplete = 0
    #singlecomplete = copy.deepcopy(grid)
    
    for i in range(len(numberlist)): #Iterates through the length of the markup of the cells 
        
        if len(numberlist[i]) == 1 and 0 not in numberlist[i]: # If the markup only contains a single value then we place this value into that cell
    
            tempval = list(numberlist[i])
            grid[i//9][i % 9] = tempval.pop()
            numberlist[i].clear()
            numberlist[i].add(0)
            numberlist = markup(grid)
            
    return grid
                    
def applycrooks(grid):
    """Applies Crook's algorithm iteratively until complete"""
    
    while True : # Infinite loop
        
        numberlist = markup(grid)
        solution_old = copy.deepcopy(grid)  #Used in future to determine if the application of Crook's algorithm induced any change to the grid
        grid = crooksrow(grid,numberlist)

        if solution_old != grid :
            numberlist = markup(grid)
            grid = crookscolumn(grid,numberlist)

        if solution_old != grid :
            numberlist = markup(grid)
            grid = crooksbox(grid,numberlist)
        
        if solution_old == grid : # If there is no change then we either have to make a guessor the grid is solved
            return grid
                


def setup(grid):
    """The primary function that initialises necessary variables and returns the solved grid or the boolean value False if a solution is not found
       This function is the only necessary function that needs to be called by the user to begin the solving process"""
    
    global total_it
    global finalgridsolved
    
    finalgridsolved = [] #Reset our final solved puzzle variable in case new puzzle is to be processed
    total_it = 0
    
    if len(grid) != 9 : #Determines if puzzle has enough columns
        raise Exception("Invalid Puzzle Entered")
    
    for i in range(9):
        if len(grid[i]) != 9 : # Determines if there are 81 cells in the puzzle arranged in sets of 9. 
            raise Exception("Invalid Puzzle Entered")
    
    solve_Rec_crook(grid)
    
    if finalgridsolved == [] : #If this is empty then the puzzle is insolvable
        return False
    else :
        return grid
    
    

