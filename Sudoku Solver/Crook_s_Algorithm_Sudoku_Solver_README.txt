Welcome to the Computational Solution to Crook's Pen and Paper algorithm Sudoku solver!!
This program implements the use of logic to solve any sudoku puzzle. 
A breakdown of functions can be found using the command line and the pydoc command.

In order to use the program one must enter a 2D array of arrays in such a way that represents a sudoku grid
Each array in the array of arrays represents a row in the sudoku grid. 
This means our valid input is in the form :

[row 1, row 2, row 3, row 4, row 5, row 6, row 7, row 8, row 9]]

each element row 1, row 2 etc represents an array of 9 elements with the value 0 indicating an empty cell. This could be something like :

[1,5,0,7,0,0,8,2,0]

This data structure for example could look something like this for an unsolved puzzle: 

[[1, 9, 0, 0, 0, 0, 0, 6, 0], [2, 0, 0, 5, 0, 0, 1, 8, 0], [0, 0, 0, 2, 9, 0, 0, 4, 7], [6, 0, 0, 7, 0, 0, 0, 5, 3],
[0, 0, 0, 8, 0, 5, 0, 0, 0], [4, 5, 0, 0, 0, 3, 0, 0, 8], [5, 4, 0, 0, 7, 8, 0, 0, 0], [0, 1, 8, 0, 0, 9, 0, 0, 5],
[0, 6, 0, 0, 0, 0, 0, 9, 0]]

In a more legibile format this looks like.

[[1, 9, 0, 0, 0, 0, 0, 6, 0],
 [2, 0, 0, 5, 0, 0, 1, 8, 0],
 [0, 0, 0, 2, 9, 0, 0, 4, 7],
 [6, 0, 0, 7, 0, 0, 0, 5, 3],
 [0, 0, 0, 8, 0, 5, 0, 0, 0],
 [4, 5, 0, 0, 0, 3, 0, 0, 8],
 [5, 4, 0, 0, 7, 8, 0, 0, 0],
 [0, 1, 8, 0, 0, 9, 0, 0, 5],
 [0, 6, 0, 0, 0, 0, 0, 9, 0]]

This looks very similar to a sudoku grid.

In order to now process your unsolved sudoku puzzle just call the function setup(grid) with grid being the variable that represents
the unsolved puzzle! If the puzzle has a solution then the solution will be returned to you in the same format it was passed into
the function as. If the puzzle has no solution then the boolean value False will be returned instead.

Happy Solving! 